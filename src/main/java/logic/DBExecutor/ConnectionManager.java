package logic.DBExecutor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {

    public Connection getConnection(String sHost, String sPort, String sDatabase, String sUser, String sPassword){
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + sHost + ":" + sPort + "/" + sDatabase, sUser, sPassword);
        } catch (SQLException sqe) {
            System.err.println("Error: " + sqe.getMessage());
        }
        return connection;
    }

    public void closeConnection(Connection connection){
        try {
            connection.close();
        } catch (SQLException sqe) {
            System.err.println("Error: " + sqe.getMessage());
        }
    }

}