package logic.DBExecutor;

public class DBData {
    //my test DB
    public final static String HOST = "localhost";
    public final static String PORT = "3306";
    public final static String DATABASE = "localDB";
    public final static String USER = "root";
    public final static String PASSWORD ="mysql";

    //work DB
//    public final static String HOST = "sql151.main-hosting.eu";
//    public final static String PORT = "3306";
//    public final static String DATABASE = "u994820835_db";
//    public final static String USER = "u994820835_db";
//    public final static String PASSWORD ="Password1234567";

    public final static String CREATE_COFFEEMACHINE_TABLE = "CREATE TABLE coffeemachine (\n" +
                                                                "  id INTEGER PRIMARY KEY AUTO_INCREMENT,\n" +
                                                                "  model TEXT,\n" +
                                                                "  modelType TEXT,\n" +
                                                                "  pressureLevel INTEGER,\n" +
                                                                "  waterCapacity DOUBLE,\n" +
                                                                "  power INTEGER,\n" +
                                                                "  manufacturer TEXT,\n" +
                                                                "  price DOUBLE\n" +
                                                                ");";

    public final static String CREATE_LAPTOP_TABLE = "CREATE TABLE laptop (\n" +
                                                            "  id INTEGER PRIMARY KEY AUTO_INCREMENT,\n" +
                                                            "  model TEXT,\n" +
                                                            "  modelType TEXT,\n" +
                                                            "  processor TEXT,\n" +
                                                            "  display TEXT,\n" +
                                                            "  RAM INTEGER,\n" +
                                                            "  videoCard TEXT,\n" +
                                                            "  OS TEXT,\n" +
                                                            "  price DOUBLE\n" +
                                                            ");";

    public final static String CREATE_COMMON_TABLE = "CREATE TABLE commonTable (\n" +
                                                        "  id INTEGER PRIMARY KEY AUTO_INCREMENT,\n" +
                                                        "  modelType TEXT,\n" +
                                                        "  model TEXT,\n" +
                                                        "  price DOUBLE\n" +
                                                        ");";

    public final static String DROP_COFFEEMACHINE_TABLE = "DROP TABLE coffeemachine;";

    public final static String DROP_COMMON_TABLE = "DROP TABLE commonTable;";

    public final static String DROP_LAPTOP_TABLE = "DROP TABLE laptop;";

}