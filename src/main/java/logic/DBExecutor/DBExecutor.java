package logic.DBExecutor;

import java.sql.*;

import static logic.DBExecutor.DBData.*;

public class DBExecutor {

    private Connection connection;

    public DBExecutor(Connection connection) {
        this.connection = connection;
    }

    //method for checking if the table exist
    public boolean tableExist(String tableName) throws SQLException {
        boolean tExists = false;
        try (ResultSet rs = connection.getMetaData().getTables(null, null, tableName, null)) {
            while (rs.next()) {
                String tName = rs.getString("TABLE_NAME");
                if (tName != null && tName.equals(tableName)) {
                    tExists = true;
                    break;
                }
            }
        }
        return tExists;
    }

    //create table method
    public void createTable(String tableName){
        Statement statement = null;

        try{
            statement = connection.createStatement();

            if(tableName.equalsIgnoreCase("Coffee"))
                statement.executeUpdate(CREATE_COFFEEMACHINE_TABLE);

            if(tableName.equalsIgnoreCase("Laptop"))
                statement.executeUpdate(CREATE_LAPTOP_TABLE);

            if(tableName.equalsIgnoreCase("Common"))
                statement.executeUpdate(CREATE_COMMON_TABLE);

            System.out.println(tableName + " created successfully...");
        }catch(SQLException sqe){
            System.err.println("Error: " + sqe.getMessage());
        }
    }

    //delete table method
    public void dropTable(String tableName){
        Statement statement = null;

        try{
            statement = connection.createStatement();

            if(tableName.equalsIgnoreCase("Coffee"))
                statement.executeUpdate(DROP_COFFEEMACHINE_TABLE);

            if(tableName.equalsIgnoreCase("Laptop"))
                statement.executeUpdate(DROP_LAPTOP_TABLE);

            if(tableName.equalsIgnoreCase("Common"))
                statement.executeUpdate(DROP_COMMON_TABLE);

            System.out.println(tableName + " deleted successfully...");
        }catch(SQLException sqe){
            System.err.println("Error: " + sqe.getMessage());
        }
    }

}