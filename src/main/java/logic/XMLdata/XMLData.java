package logic.XMLdata;

public class XMLData {

    //path to coffeemachine file
    public final static String coffeeXMLFile = "src/main/java/logic/XMLdata/CoffeeMachines.xml";

    //path to laptop file
    public final static String laptopXML = "src/main/java/logic/XMLdata/Laptops.xml";

}
