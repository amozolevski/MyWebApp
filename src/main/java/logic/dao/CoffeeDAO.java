package logic.dao;

import logic.entity.CoffeeMachine;
import org.hibernate.Session;

import java.util.List;

public interface CoffeeDAO {

    List<CoffeeMachine> getCoffeeMachineListFromDB(Session session);
    void insertCoffeeMachineToDB(Session session, List<CoffeeMachine> coffeeMachineList);

}
