package logic.dao;

import logic.entity.CommonProduct;
import org.hibernate.Session;

import java.util.List;

public interface CommonProductDAO {
    List<CommonProduct> getProductsList(Session session);
}
