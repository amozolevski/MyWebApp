package logic.dao;

import logic.entity.Laptop;
import org.hibernate.Session;

import java.util.List;

public interface LaptopDAO {
    List<Laptop> getLaptopListFromDB(Session session);
    void insertLaptopListToDB(Session session, List<Laptop> laptopList);
}
