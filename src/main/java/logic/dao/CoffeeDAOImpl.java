package logic.dao;

import logic.entity.CoffeeMachine;
import org.hibernate.Session;

import java.util.List;

public class CoffeeDAOImpl implements CoffeeDAO{
    @Override
    public List<CoffeeMachine> getCoffeeMachineListFromDB(Session session) {
        return session.createQuery("from CoffeeMachine").list();
    }

    @Override
    public void insertCoffeeMachineToDB(Session session, List<CoffeeMachine> coffeeMachineList) {
        for (CoffeeMachine c : coffeeMachineList) {
            session.beginTransaction();
            session.save(c);
            session.getTransaction().commit();
        }
    }
}
