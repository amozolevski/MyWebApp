package logic.dao;

import logic.entity.CoffeeMachine;
import logic.entity.CommonProduct;
import logic.entity.Laptop;
import org.hibernate.Session;

import java.util.List;

public class CommonProductDAOImpl implements CommonProductDAO {
    @Override
    public List<CommonProduct> getProductsList(Session session) {
        return session.createQuery("from CommonProduct").list();
    }

    public void insertProductsFromCoffee(Session session, List<CoffeeMachine> coffeeMachineList){
        for (CoffeeMachine c : coffeeMachineList) {
            CommonProduct commonProduct = new CommonProduct();
            commonProduct.setModelType(c.getModelType());
            commonProduct.setModel(c.getModel());
            commonProduct.setPrice(c.getPrice());

            session.beginTransaction();
            session.save(commonProduct);
            session.getTransaction().commit();
        }
    }

    public void insertProductsFromLaptop(Session session, List<Laptop> laptopList){
        for (Laptop l : laptopList) {
            CommonProduct commonProduct = new CommonProduct();
            commonProduct.setModelType(l.getModelType());
            commonProduct.setModel(l.getModel());
            commonProduct.setPrice(l.getPrice());

            session.beginTransaction();
            session.save(commonProduct);
            session.getTransaction().commit();
        }
    }
}