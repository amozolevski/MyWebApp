package logic.dao;

import logic.entity.Laptop;
import org.hibernate.Session;

import java.util.List;

public class LaptopDAOImpl implements LaptopDAO {
    @Override
    public List<Laptop> getLaptopListFromDB(Session session) {
        return session.createQuery("from Laptop").list();
    }

    @Override
    public void insertLaptopListToDB(Session session, List<Laptop> laptopList) {
        for (Laptop l : laptopList) {
            session.beginTransaction();
            session.save(l);
            session.getTransaction().commit();
        }
    }
}
