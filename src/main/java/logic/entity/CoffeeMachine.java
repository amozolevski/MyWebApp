package logic.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "coffeemachine")
public class CoffeeMachine implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "model", nullable = false)
    private String model;

    @Column(name = "modelType", nullable = false)
    private String modelType;

    @Column(name = "pressureLevel", nullable = false)
    private int pressureLevel;

    @Column(name = "waterCapacity", nullable = false)
    private double waterCapacity;

    @Column(name = "power", nullable = false)
    private int power;

    @Column(name = "manufacturer", nullable = false)
    private String country;

    @Column(name = "price", nullable = false)
    private double price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPressureLevel() {
        return pressureLevel;
    }

    public void setPressureLevel(int pressureLevel) {
        this.pressureLevel = pressureLevel;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModelType() {
        return modelType;
    }

    public void setModelType(String modelType) {
        this.modelType = modelType;
    }

    public double getWaterCapacity() {
        return waterCapacity;
    }

    public void setWaterCapacity(double waterCapacity) {
        this.waterCapacity = waterCapacity;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "model = " + model +
                ", modelType = " + modelType +
                ", pressureLevel " + pressureLevel +
                ", waterCapacity = " + waterCapacity +
                ", power = " + power +
                ", country = " + country +
                ", price = " + price;
    }

}