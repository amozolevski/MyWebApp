package logic.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "laptop")
public class Laptop implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "model", nullable = false)
    private String model;

    @Column(name = "modelType", nullable = false)
    private String modelType;

    @Column(name = "processor", nullable = false)
    private String processor;

    @Column(name = "display", nullable = false)
    private String display;

    @Column(name = "RAM", nullable = false)
    private int ram;

    @Column(name = "videoCard", nullable = false)
    private String videoCard;

    @Column(name = "OS", nullable = false)
    private String osType;

    @Column(name = "price", nullable = false)
    private double price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModelType() {
        return modelType;
    }

    public void setModelType(String modelType) {
        this.modelType = modelType;
    }

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public String getVideoCard() {
        return videoCard;
    }

    public void setVideoCard(String videoCard) {
        this.videoCard = videoCard;
    }

    public String getOsType() {
        return osType;
    }

    public void setOsType(String osType) {
        this.osType = osType;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "model = " + model +
                ", modelType = " + modelType +
                ", processor = " + processor +
                ", display = " + display +
                ", ram = " + ram +
                ", videoCard = " + videoCard +
                ", osType = " + osType +
                ", price = " + price;
    }
}
