package logic.entity.CommonProductComparators;

import logic.entity.CommonProduct;

import java.util.Comparator;

public class PriceComparator implements Comparator<CommonProduct> {
    @Override
    public int compare(CommonProduct o1, CommonProduct o2) {
        if(o1.getPrice() < o2.getPrice())
            return -1;

        if(o1.getPrice() > o2.getPrice())
            return 1;

        return 0;
    }
}
