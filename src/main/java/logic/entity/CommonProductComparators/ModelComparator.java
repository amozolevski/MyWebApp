package logic.entity.CommonProductComparators;

import logic.entity.CommonProduct;

import java.util.Comparator;

public class ModelComparator implements Comparator<CommonProduct> {
    @Override
    public int compare(CommonProduct o1, CommonProduct o2) {
        return o1.getModel().compareTo(o2.getModel());
    }
}
