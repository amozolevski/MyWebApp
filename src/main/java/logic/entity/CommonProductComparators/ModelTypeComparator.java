package logic.entity.CommonProductComparators;

import logic.entity.CommonProduct;

import java.util.Comparator;

public class ModelTypeComparator implements Comparator<CommonProduct> {

    @Override
    public int compare(CommonProduct o1, CommonProduct o2) {
        return o1.getModelType().compareTo(o2.getModelType());
    }
}
