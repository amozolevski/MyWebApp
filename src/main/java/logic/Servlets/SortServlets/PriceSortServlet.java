package logic.Servlets.SortServlets;

import logic.dao.CommonProductDAOImpl;
import logic.entity.CommonProduct;
import logic.entity.CommonProductComparators.ModelComparator;
import logic.entity.CommonProductComparators.PriceComparator;
import logic.persistense.HibernateUtil;
import org.hibernate.Session;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

public class PriceSortServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Session session = HibernateUtil.getSession().openSession();

        CommonProductDAOImpl commonProductDAO = new CommonProductDAOImpl();

        List<CommonProduct> list = new ArrayList<>();
        list.addAll(commonProductDAO.getProductsList(session));
        Collections.sort(list, new PriceComparator());

        req.setAttribute("priceSort", list);

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("views/SortPages/PriceSort.jsp");
        requestDispatcher.forward(req, resp);

    }
}
