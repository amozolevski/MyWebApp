package logic.Servlets;

import logic.DBExecutor.ConnectionManager;
import logic.DBExecutor.DBExecutor;
import logic.persistense.HibernateUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import static logic.DBExecutor.DBData.*;
import static logic.DBExecutor.DBData.PASSWORD;

public class CloseServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ConnectionManager connectionManager = new ConnectionManager();
        Connection connection = connectionManager.getConnection(HOST, PORT, DATABASE, USER, PASSWORD);

        DBExecutor dbExecutor = new DBExecutor(connection);

        try {
            if (dbExecutor.tableExist("coffeemachine"))
                dbExecutor.dropTable("coffee");

            if (dbExecutor.tableExist("laptop"))
                dbExecutor.dropTable("laptop");

            if(dbExecutor.tableExist("commonTable"))
                dbExecutor.dropTable("common");

        } catch (SQLException sqe){
            System.err.println("Error: " + sqe.getMessage());
        } finally {
            connectionManager.closeConnection(connection);
        }

        HibernateUtil.shutDownSession();

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("views/Close.jsp");
        requestDispatcher.forward(req, resp);
    }

}