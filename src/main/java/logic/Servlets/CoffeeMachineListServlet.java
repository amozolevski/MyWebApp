package logic.Servlets;

import logic.DBExecutor.ConnectionManager;
import logic.DBExecutor.DBExecutor;
import logic.XMLdata.XMLData;
import logic.XMLparsers.CoffeeMachineXMLParser;
import logic.dao.CoffeeDAOImpl;
import logic.dao.CommonProductDAOImpl;
import logic.entity.CoffeeMachine;
import logic.persistense.HibernateUtil;
import org.hibernate.Session;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static logic.DBExecutor.DBData.*;

public class CoffeeMachineListServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<CoffeeMachine> coffeeMachineList = CoffeeMachineXMLParser.parseCoffeeToList(XMLData.coffeeXMLFile);

        CoffeeDAOImpl coffeeDAO = new CoffeeDAOImpl();

        ConnectionManager connectionManager = new ConnectionManager();
        Connection connection = connectionManager.getConnection(HOST, PORT, DATABASE, USER, PASSWORD);

        DBExecutor dbExecutor = new DBExecutor(connection);

        Session session = HibernateUtil.getSession().openSession();

        try {
            if (!dbExecutor.tableExist("coffeemachine"))
                dbExecutor.createTable("coffee");

            if(!dbExecutor.tableExist("commonTable"))
                dbExecutor.createTable("common");

        } catch (SQLException sqe){
            System.err.println("Error: " + sqe.getMessage());
        } finally {
            connectionManager.closeConnection(connection);
        }

        coffeeDAO.insertCoffeeMachineToDB(session, coffeeMachineList);

        CommonProductDAOImpl commonProductDAO = new CommonProductDAOImpl();
        commonProductDAO.insertProductsFromCoffee(session, coffeeDAO.getCoffeeMachineListFromDB(session));

        req.setAttribute("coffeeMachineList", commonProductDAO.getProductsList(session));

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("views/CoffeeMachineList.jsp");
        requestDispatcher.forward(req, resp);
    }
}