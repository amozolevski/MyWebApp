package logic.Servlets;

import logic.DBExecutor.ConnectionManager;
import logic.DBExecutor.DBExecutor;
import logic.XMLdata.XMLData;
import logic.XMLparsers.LaptopXMLParser;
import logic.dao.CommonProductDAOImpl;
import logic.dao.LaptopDAOImpl;
import logic.entity.Laptop;
import logic.persistense.HibernateUtil;
import org.hibernate.Session;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static logic.DBExecutor.DBData.*;
import static logic.DBExecutor.DBData.PASSWORD;

public class LaptopListServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<Laptop> laptopList = LaptopXMLParser.parseLaptopToList(XMLData.laptopXML);

        LaptopDAOImpl laptopDAO = new LaptopDAOImpl();

        ConnectionManager connectionManager = new ConnectionManager();
        Connection connection = connectionManager.getConnection(HOST, PORT, DATABASE, USER, PASSWORD);

        DBExecutor dbExecutor = new DBExecutor(connection);

        Session session = HibernateUtil.getSession().openSession();

        try {
            if (!dbExecutor.tableExist("laptop"))
                dbExecutor.createTable("laptop");

            if (!dbExecutor.tableExist("commonTable"))
                dbExecutor.createTable("common");

        } catch (SQLException sqe) {
            System.err.println("Error: " + sqe.getMessage());
        } finally {
            connectionManager.closeConnection(connection);
        }

        laptopDAO.insertLaptopListToDB(session, laptopList);

        CommonProductDAOImpl commonProductDAO = new CommonProductDAOImpl();
        commonProductDAO.insertProductsFromLaptop(session, laptopDAO.getLaptopListFromDB(session));

        req.setAttribute("laptopList", commonProductDAO.getProductsList(session));

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("views/LaptopList.jsp");
        requestDispatcher.forward(req, resp);
    }
}