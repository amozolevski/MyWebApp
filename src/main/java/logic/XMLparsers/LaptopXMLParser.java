package logic.XMLparsers;

import logic.entity.Laptop;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class LaptopXMLParser{

    public static List<Laptop> parseLaptopToList(String filePath){

        Laptop laptop = null;
        List<Laptop> laptopList = new ArrayList<>();
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

        try{
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(filePath));

            while (xmlEventReader.hasNext()){
                XMLEvent xmlEvent = xmlEventReader.nextEvent();

                if(xmlEvent.isStartElement()){
                    StartElement startElement = xmlEvent.asStartElement();

                    if(startElement.getName().getLocalPart().equalsIgnoreCase("Laptop")){
                        laptop = new Laptop();
                    }

                    if(startElement.getName().getLocalPart().equalsIgnoreCase("Model")){
                        xmlEvent = xmlEventReader.nextEvent();
                        laptop.setModel(xmlEvent.asCharacters().getData());
                    }

                    if(startElement.getName().getLocalPart().equalsIgnoreCase("ModelType")){
                        xmlEvent = xmlEventReader.nextEvent();
                        laptop.setModelType(xmlEvent.asCharacters().getData());
                    }

                    if(startElement.getName().getLocalPart().equalsIgnoreCase("Processor")){
                        xmlEvent = xmlEventReader.nextEvent();
                        laptop.setProcessor(xmlEvent.asCharacters().getData());
                    }

                    if(startElement.getName().getLocalPart().equalsIgnoreCase("Display")){
                        xmlEvent = xmlEventReader.nextEvent();
                        laptop.setDisplay(xmlEvent.asCharacters().getData());
                    }

                    if(startElement.getName().getLocalPart().equalsIgnoreCase("RAM")){
                        xmlEvent = xmlEventReader.nextEvent();
                        laptop.setRam(Integer.parseInt(xmlEvent.asCharacters().getData()));
                    }

                    if(startElement.getName().getLocalPart().equalsIgnoreCase("VideoCard")){
                        xmlEvent = xmlEventReader.nextEvent();
                        laptop.setVideoCard(xmlEvent.asCharacters().getData());
                    }

                    if(startElement.getName().getLocalPart().equalsIgnoreCase("OS")){
                        xmlEvent = xmlEventReader.nextEvent();
                        laptop.setOsType(xmlEvent.asCharacters().getData());
                    }

                    if(startElement.getName().getLocalPart().equalsIgnoreCase("Price")){
                        xmlEvent = xmlEventReader.nextEvent();
                        laptop.setPrice(Double.parseDouble(xmlEvent.asCharacters().getData()));
                    }
                }

                if(xmlEvent.isEndElement()){
                    EndElement endElement = xmlEvent.asEndElement();

                    if(endElement.getName().getLocalPart().equalsIgnoreCase("Laptop")){
                        laptopList.add(laptop);
                    }
                }
            }

        } catch (FileNotFoundException fnfe){
            System.err.println("Error: file not found " + fnfe.getMessage());
        } catch (XMLStreamException xse) {
            System.err.println("Error: " + xse.getMessage());
        }

        return laptopList;
    }
}