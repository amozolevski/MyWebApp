package logic.XMLparsers;

import logic.entity.CoffeeMachine;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class CoffeeMachineXMLParser{

    public static List<CoffeeMachine> parseCoffeeToList(String filePath){

        CoffeeMachine coffeeMachine = null;
        List<CoffeeMachine> coffeeMachineList = new ArrayList<>();
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

        try{
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(filePath));

            while (xmlEventReader.hasNext()){
                XMLEvent xmlEvent = xmlEventReader.nextEvent();

                if(xmlEvent.isStartElement()){
                    StartElement startElement = xmlEvent.asStartElement();

                    if(startElement.getName().getLocalPart().equalsIgnoreCase("CoffeeMachine")){
                        coffeeMachine = new CoffeeMachine();
                    }

                    if(startElement.getName().getLocalPart().equalsIgnoreCase("Model")){
                        xmlEvent = xmlEventReader.nextEvent();
                        coffeeMachine.setModel(xmlEvent.asCharacters().getData());
                    }

                    if(startElement.getName().getLocalPart().equalsIgnoreCase("ModelType")){
                        xmlEvent = xmlEventReader.nextEvent();
                        coffeeMachine.setModelType(xmlEvent.asCharacters().getData());
                    }

                    if(startElement.getName().getLocalPart().equalsIgnoreCase("PressureLevel")){
                        xmlEvent = xmlEventReader.nextEvent();
                        coffeeMachine.setPressureLevel(Integer.parseInt(xmlEvent.asCharacters().getData()));
                    }

                    if(startElement.getName().getLocalPart().equalsIgnoreCase("WaterCapacity")){
                        xmlEvent = xmlEventReader.nextEvent();
                        coffeeMachine.setWaterCapacity(Double.parseDouble(xmlEvent.asCharacters().getData()));
                    }

                    if(startElement.getName().getLocalPart().equalsIgnoreCase("Power")){
                        xmlEvent = xmlEventReader.nextEvent();
                        coffeeMachine.setPower(Integer.parseInt(xmlEvent.asCharacters().getData()));
                    }

                    if(startElement.getName().getLocalPart().equalsIgnoreCase("Manufacturer")){
                        xmlEvent = xmlEventReader.nextEvent();
                        coffeeMachine.setCountry(xmlEvent.asCharacters().getData());
                    }

                    if(startElement.getName().getLocalPart().equalsIgnoreCase("Price")){
                        xmlEvent = xmlEventReader.nextEvent();
                        coffeeMachine.setPrice(Double.parseDouble(xmlEvent.asCharacters().getData()));
                    }
                }

                if(xmlEvent.isEndElement()){
                    EndElement endElement = xmlEvent.asEndElement();

                    if(endElement.getName().getLocalPart().equalsIgnoreCase("CoffeeMachine")){
                        coffeeMachineList.add(coffeeMachine);
                    }
                }
            }

        } catch (FileNotFoundException fnfe) {
            System.err.println("Error: file not found " + fnfe.getMessage());
        } catch (XMLStreamException xse) {
            System.err.println("Error: " + xse.getMessage());
        }

        return coffeeMachineList;
    }

}