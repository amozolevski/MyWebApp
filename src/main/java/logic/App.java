package logic;

import logic.DBExecutor.ConnectionManager;
import logic.DBExecutor.DBExecutor;
import logic.XMLdata.XMLData;
import logic.XMLparsers.CoffeeMachineXMLParser;
import logic.XMLparsers.LaptopXMLParser;
import logic.dao.CoffeeDAOImpl;
import logic.dao.CommonProductDAOImpl;
import logic.dao.LaptopDAOImpl;
import logic.entity.CoffeeMachine;
import logic.entity.Laptop;
import logic.persistense.HibernateUtil;
import org.hibernate.Session;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static logic.DBExecutor.DBData.*;

public class App {
    public static void main(String[] args) {

        //Test back-end with Laptop list

        List<CoffeeMachine> coffeeMachineList = CoffeeMachineXMLParser.parseCoffeeToList(XMLData.coffeeXMLFile);
        List<Laptop> laptopList = LaptopXMLParser.parseLaptopToList(XMLData.laptopXML);

        CoffeeDAOImpl coffeeDAO = new CoffeeDAOImpl();
        LaptopDAOImpl laptopDAO = new LaptopDAOImpl();

        ConnectionManager connectionManager = new ConnectionManager();
        Connection connection = connectionManager.getConnection(HOST, PORT, DATABASE, USER, PASSWORD);

        DBExecutor dbExecutor = new DBExecutor(connection);

        Session session = HibernateUtil.getSession().openSession();

        try {
            if (!dbExecutor.tableExist("coffeemachine"))
                dbExecutor.createTable("coffee");

            if (!dbExecutor.tableExist("laptop"))
                dbExecutor.createTable("laptop");

            if(!dbExecutor.tableExist("commonTable"))
                dbExecutor.createTable("common");

        } catch (SQLException sqe){
            System.err.println("Error: " + sqe.getMessage());
        } finally {
            connectionManager.closeConnection(connection);
        }

        coffeeDAO.insertCoffeeMachineToDB(session, coffeeMachineList);
        laptopDAO.insertLaptopListToDB(session, laptopList);

        CommonProductDAOImpl commonProductDAO = new CommonProductDAOImpl();
        commonProductDAO.insertProductsFromCoffee(session, coffeeDAO.getCoffeeMachineListFromDB(session));
        commonProductDAO.getProductsList(session).forEach(System.out::println);

        HibernateUtil.shutDownSession();
    }
}