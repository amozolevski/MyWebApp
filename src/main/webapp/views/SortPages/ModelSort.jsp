<%@ page import="java.util.*" %>
<%@ page import="logic.entity.CommonProduct" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <meta charset="UTF-8">
        <title>My WEB App</title>
    </head>

    <body>
        <div>
            <h1>My WEB App</h1>
        </div>

        <div>
            <button onclick="location.href='/Close'">Close tab</button>
        </div>

        <div>
            <h2>Sorted list:</h2>
            <h1></h1>
            <h1></h1>
            <h1></h1>
        </div>

        <div>
            <button onclick="location.href='/ModelTypeSort'">by model type</button>
            <button onclick="location.href='/ModelSort'">by model </button>
            <button onclick="location.href='/PriceSort'">by price</button>
        </div>

        <div>
            <h3>Sorted by model:</h3>
        </div>

        <div>
            <div>
                <%
                    List<CommonProduct> list = (List<CommonProduct>) request.getAttribute("modelSort");

                    if (list != null && !list.isEmpty()) {
                        out.print("<table border=\"1\">");
                        out.print("<tr>");
                        out.print("<th>Model Type</th>");
                        out.print("<th>Model</th>");
                        out.print("<th>Price</th>");
                        out.print("</tr>");
                        for(int i=0; i < list.size(); i++){
                            out.print("<tr>");
                            out.print("<td>" + list.get(i).getModelType() + "</td>");
                            out.print("<td>" + list.get(i).getModel() + "</td>");
                            out.print("<td>" + list.get(i).getPrice() + "</td>");
                            out.print("</tr>");
                        }
                    } else out.println("<p>There are no data yet!</p>");
                %>
            </div>
        </div>

    </body>
</html>